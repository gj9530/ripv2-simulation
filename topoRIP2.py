from mininet.net import Mininet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.log import setLogLevel, info

def emptyNet():
    "Create an empty network and add nodes to it."

    net = Mininet()

    info('*** Adding controller\n')
    c0 = net.addController('c0')

    info('*** Adding hosts\n')
    h1 = net.addHost('h1', ip="10.0.0.1/24")
    h2 = net.addHost('h2', ip="10.0.0.2/24")
    h3 = net.addHost('h3', ip="10.0.0.3/24")
    h4 = net.addHost('h4', ip="10.0.0.4/24")
    h5 = net.addHost('h5', ip="10.0.0.5/24")

    info('*** Adding Switches\n')
    s1 = net.addSwitch('s1', ip="10.0.0.11")
    # s2 = net.addSwitch('s2', ip="10.0.0.22")
    # s3 = net.addSwitch('s3', ip="10.0.0.33")
    # s4 = net.addSwitch('s4', ip="10.0.0.44")
    # s5 = net.addSwitch('s5', ip="10.0.0.55")


    info('*** Creating links\n')
    # net.addLink(h1,h2)
    # net.addLink(h3,h2)

    net.addLink(h1,s1)
    net.addLink(h2,s1)
    net.addLink(h3,s1)
    net.addLink(h4,s1)
    net.addLink(h5,s1)

    # net.addLink(s1,s5)
    info( '*** Starting network\n')
    net.start()

    info('*** Starting CLI\n')
    CLI( net )

    info('***Stopping network')
    net.stop()

if __name__ == '__main__':
	setLogLevel( 'info' )
	emptyNet()
