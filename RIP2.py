import socket
import threading
import time
import struct

from configparser import ConfigParser
RIPPORT = 520
MIN_METRIC = 0
MAX_METRIC = 15
INFINITY = 16
INTERVAL = 2
TIMEOUT = INTERVAL * 2
COMMAND = 2
VERSION = 2
UNUSED = 0
AFI = 2 # Address Family Identifier
ROUTETAG = 0
HEADER_SIZE = 4
HEADER_FORMAT = '!BBH'
ENTRY_SIZE = 20
ENTRY_FORMAT = '!HHIIII'
MAX_NUM_ENTRY= 25

CIDR_NEXTHOP = {} # The dictionary that stores the cidr address, nextHop that gives the lowest metric. Format {cidr:[nextHop, metric]}
NEXTHOP_TIME = {}

class RIPPacket:
	def __init__(self, entries = [], command = COMMAND, version = VERSION, unused = UNUSED):
		self.command = command
		self.version = version
		self.unused = unused
		self.entries = entries

	def serialize(self):
		header = struct.pack(HEADER_FORMAT, self.command, self.version, self.unused)
		entry_bin = b''
		for entry in self.entries:
			entry_bin = entry_bin + entry.serialize()
		return header + entry_bin

	def deserialize(self, data):
		header = data[:HEADER_SIZE]
		entry_bin = data[HEADER_SIZE:]
		command, version, unused = struct.unpack(HEADER_FORMAT, header)
		entries = []
		for i in range(0,len(entry_bin)/ENTRY_SIZE):
			entry = entry_bin[ i * ENTRY_SIZE : (i + 1) * ENTRY_SIZE]
			afi, routeTag, ip, subnetMask, nextHop, metric = struct.unpack(ENTRY_FORMAT, entry)
			unpacked_entry = RIPEntry(ip, subnetMask, metric, nextHop, afi, routeTag)
			entries.append(unpacked_entry)

		return command, version, unused, entries

	def addEntry (self, entry):
		self.entries.append(entry)

	def removeEntry (self, entry):
		for e in self.entries:
			if e.ip == entry.ip and e.subnetMask == entry.subnetMask and e.nextHop == entry.nextHop:
				self.entries.remove(e)

	def update(self, cidr):
		for e in self.entries:
			if netmask_to_cidr(e.ip, e.subnetMask) == cidr:
				e.nextHop = ip2int(CIDR_NEXTHOP[cidr][0])
				e.metric = CIDR_NEXTHOP[cidr][1]
				printTable(self)

	def reRoute(self, nextHop_addr):
		for cidr, nextHop_metric in CIDR_NEXTHOP.iteritems():
			stored_nextHop = nextHop_metric[0]
			metric = nextHop_metric[1]
			if stored_nextHop == nextHop_addr:
				CIDR_NEXTHOP[cidr] = [stored_nextHop, INFINITY]
		for e in self.entries:
			if int2ip(e.nextHop) == nextHop_addr:
				e.metric = INFINITY
				printTable(self)


class RIPRouting (threading.Thread):
	RIPPACKET = RIPPacket()
	def __init__(self):
	    super(RIPRouting,self).__init__()

	def startRIP(self, cidr):
		addr, netmask = cidr_to_netmask(cidr)
		ip = ip2int(addr)
		subnetMask = ip2int(netmask)
		metric = 0
		entry = RIPEntry(ip, subnetMask, metric)
		RIPRouting.RIPPACKET.addEntry(entry)
		CIDR_NEXTHOP[cidr] = ['0.0.0.0', metric]
		printTable(RIPRouting.RIPPACKET)


	def transport(self, id, srcIP, srcPort, dstIP, dstPort):   
	    if id == 1:
	        sender = Sender()
	        sender.sendPacket(srcIP, srcPort, dstIP, dstPort)
	        
	    else:
	        receiver = Receiver()
	        receiver.receivePacket(srcIP, srcPort)

class Sender:
    def sendPacket(self, srcIP, srcPort, dstIP, dstPort):
        while True:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((srcIP, srcPort))
            data = RIPRouting.RIPPACKET.serialize()
            sock.sendto(data,(dstIP, dstPort))
            sock.close()
            time.sleep(INTERVAL)
                
class Receiver:
    def receivePacket(self, srcIP, srcPort):

    	connected = False
    	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((srcIP, srcPort))
        while True:
            data,addr_port = sock.recvfrom(HEADER_SIZE + ENTRY_SIZE * MAX_NUM_ENTRY)
            connected = True
            nextHop_addr = addr_port[0]
            ownHop_addr = srcIP
            command , version, unused, entries = RIPPacket().deserialize(data)
            pkt = RIPPacket(entries)
            NEXTHOP_TIME[nextHop_addr] = time.time()
            parse_pkt(pkt, ownHop_addr, nextHop_addr)


class RIPEntry:

 	def __init__(self, ip , subnetMask, metric, nextHop = 0, afi = AFI, routeTag = ROUTETAG):
 		self.afi = afi
 		self.routeTag = routeTag
 		self.ip = ip
 		self.subnetMask = subnetMask
 		self.nextHop = nextHop
 		self.metric = metric

 	def serialize(self):
 		return struct.pack(ENTRY_FORMAT, self.afi, self.routeTag, self.ip, self.subnetMask, self.nextHop, self.metric)

class Timer (threading.Thread):
	def __init__(self):
		super(Timer,self).__init__()

	def checkTime(self):
		while True:
			for nextHop_addr, t in NEXTHOP_TIME.iteritems():
				# print 'time=', time.time() - t
				if time.time() - t > TIMEOUT:
					NEXTHOP_TIME.pop(nextHop_addr)
					print('Timeout!')
					RIPRouting.RIPPACKET.reRoute(nextHop_addr)
					break

			time.sleep(TIMEOUT)





def cidr_to_netmask(cidr):
	cidr_split = cidr.split('/')
	network = cidr_split[0]
	if len(cidr_split) < 2:
		net_bits = 32
	else:
		net_bits = cidr_split[1]
	host_bits = 32 - int(net_bits)
	netmask = socket.inet_ntoa(struct.pack('!I', (1 << 32) - (1 << host_bits)))
	return network, netmask

def netmask_to_cidr(ip, subnetMask):
	addr = int2ip(ip)
	netmask = int2ip(subnetMask)
	prefixLen = sum([bin(int(x)).count('1') for x in netmask.split('.')])
	cidr = addr + '/' + str(prefixLen)
	return cidr

def ip2int(ip):
    return struct.unpack("!I", socket.inet_aton(ip))[0]

def int2ip(num):
    return socket.inet_ntoa(struct.pack("!I", num))  

def printTable(pkt):
	print '------------------------------------------------------------------'
	print '{:^15} {:^15} {:^22} {:^15}'.format('IP', 'Subnet Mask', 'Next Hop', 'Metric')
	entries= pkt.entries
	for entry in entries:
		addr = int2ip(entry.ip)
		netmask = int2ip(entry.subnetMask)
		cidr = netmask_to_cidr(entry.ip, entry.subnetMask)		
		nextHop = CIDR_NEXTHOP[cidr][0]
		metric = entry.metric
		print '{:^15} {:^15} {:^22} {:^15}'.format(addr, netmask, nextHop, metric)
	print '------------------------------------------------------------------\n\n'

def parse_pkt(pkt, ownHop_addr, nextHop_addr):
	for entry in pkt.entries:
		cidr = netmask_to_cidr(entry.ip, entry.subnetMask)
		metric = min(entry.metric + 1, INFINITY)
		if entry.nextHop == ip2int(ownHop_addr):
			metric = INFINITY

		if cidr not in CIDR_NEXTHOP:
			CIDR_NEXTHOP[cidr] = [nextHop_addr, metric]
			newEntry = RIPEntry(entry.ip, entry.subnetMask, metric, ip2int(nextHop_addr))
			RIPRouting.RIPPACKET.addEntry(newEntry)
			printTable(RIPRouting.RIPPACKET)

		if cidr in CIDR_NEXTHOP:
			if CIDR_NEXTHOP[cidr][0] != nextHop_addr and CIDR_NEXTHOP[cidr][1] > metric:
				CIDR_NEXTHOP[cidr] = [nextHop_addr, metric]
				RIPRouting.RIPPACKET.update(cidr)
				printTable(RIPRouting.RIPPACKET)

			if CIDR_NEXTHOP[cidr][0] == nextHop_addr and CIDR_NEXTHOP[cidr][1] != metric:
				CIDR_NEXTHOP[cidr] = [nextHop_addr, metric]
				RIPRouting.RIPPACKET.update(cidr)
				printTable(RIPRouting.RIPPACKET)


     
def main():
	filename = raw_input('Enter the file name\n')
	parser = ConfigParser()
	parser.read(filename)
	link = parser.get('topo', 'link')
	links = link.split(' ')
	cidr = parser.get('topo', 'network')
	router = RIPRouting()
	router.startRIP(cidr)
	srcIP, mask = cidr_to_netmask(cidr)

	for c, dstIP in enumerate(links):
		thread1 = threading.Thread( target = RIPRouting().transport, args = (1, srcIP, RIPPORT, dstIP, RIPPORT))
		thread2 = threading.Thread( target = RIPRouting().transport, args = (2, srcIP, RIPPORT,dstIP, RIPPORT))
		thread1.start()
		thread2.start()

	thread3 = threading.Thread( target = Timer().checkTime, args = ())
	thread3.start()
    
    
    
if __name__ == "__main__":
    main()


