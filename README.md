RIP (Routing Information Protocol) is a distance-vector routing protocol, which employ the hop count as a routing metric.  

This program implements partial functionalities of the RIP version 2 protocol as described in RFC 2453 section 3.4, 4.3 and 4.4, including:  
* Convert CIDRs to IP addresses and subnet masks and vice versa.
* Solve the count-to-infinity problem using the poison reverse algorithm.
* Receive routing tables from neighbor nodes.  
* Create and update the routing tables.  